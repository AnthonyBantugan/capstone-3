import { Carousel, Container } from "react-bootstrap";

export default function Carousels() {
	return (
		<Container>
			<Carousel className="my-3 ">
				<Carousel.Item>
					<img
						className="d-block w-100 "
						src="https://cdn.shopify.com/s/files/1/2227/7667/t/2/assets/slide-image-2.jpg?v=690275897473935553"
						alt="First slide"
					/>
				</Carousel.Item>

				<Carousel.Item>
					<img
						className="d-block w-100 "
						src="https://cdn.shopify.com/s/files/1/2227/7667/t/2/assets/slide-image-4.jpg?v=10758947621012156727"
						alt="Second slide"
					/>
				</Carousel.Item>

				<Carousel.Item>
					<img
						className="d-block w-100 "
						src="https://cdn.shopify.com/s/files/1/2227/7667/t/2/assets/slide-image-3.jpg?v=16144499162912300179"
						alt="Third slide"
					/>
				</Carousel.Item>
			</Carousel>
		</Container>
	);
}
