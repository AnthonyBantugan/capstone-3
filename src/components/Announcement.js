import React from "react";
import { Container } from "react-bootstrap";

const Announcement = () => {
	return (
		<Container fluid className=" p-1 text-center bg-danger">
			<h1 className="">4.4 SALE! Up to 50% off on selected products!</h1>
		</Container>
	);
};

export default Announcement;
