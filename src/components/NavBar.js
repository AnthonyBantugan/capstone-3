import { useContext, useEffect } from "react";
import { Navbar, Nav, NavDropdown } from "react-bootstrap";
import { Link } from "react-router-dom";
import UserContext from "../UserContext";

const NavBar = () => {
	const { badge, setBadge, user, setUser } = useContext(UserContext);

	useEffect(() => {
		const loggedInUser = localStorage.getItem("user");
		if (loggedInUser) {
			const foundUser = JSON.parse(loggedInUser);
			setUser(foundUser);
		}
	}, [setUser]);

	useEffect(() => {
		const cart = JSON.parse(localStorage.getItem("cart"));
		if (cart !== null) {
			setBadge(cart.length);
		}
	}, [badge, setBadge]);

	return (
		<Navbar bg="dark" variant="dark" expand="lg" className="p-3">
			<Navbar.Brand as={Link} to="/">
				<i className="bi bi-cpu"></i>
				<span> Tech Labs</span>
			</Navbar.Brand>
			<Navbar.Toggle aria-controls="basic-navbar-nav" />
			<Navbar.Collapse id="basic-navbar-nav">
				<Nav className="ms-auto fs-5">
					{user === undefined ? (
						<>
							<Nav.Link
								as={Link}
								to="/register"
								className="text-white"
							>
								Sign-Up
							</Nav.Link>
							<Nav.Link
								as={Link}
								to="/login"
								className="text-white"
							>
								Login
							</Nav.Link>
							<Nav.Link
								as={Link}
								to="/products"
								className="text-white"
							>
								Explore
							</Nav.Link>
						</>
					) : (
						<>
							{!user.isAdmin && (
								<Nav.Link
									as={Link}
									to="/products"
									className="text-white me-3 border border-white rounded-pill px-3 fw-bold"
								>
									<i className="bi bi-bag-heart me-1 "></i>{" "}
									SHOP
								</Nav.Link>
							)}

							<Nav.Link
								as={Link}
								to={`/profile/${user._id}`}
								className="text-white"
							>
								<i className="bi bi-person-circle me-2"></i>
								{user.username}
							</Nav.Link>
							{!user.isAdmin && (
								<Nav.Link as={Link} to="/cart">
									{badge > 0 ? (
										<i className="bi bi-cart4 p-1 position-relative text-white">
											<span className="position-absolute top-5 start-100 translate-middle badge rounded-pill bg-danger ">
												{badge}
												<span className="visually-hidden"></span>
											</span>
										</i>
									) : (
										<i className="bi bi-cart4  p-2 text-white"></i>
									)}
								</Nav.Link>
							)}
							<NavDropdown
								align="end"
								title=""
								id="basic-nav-dropdown"
								variant="dark"
								className="text-white"
							>
								{user.isAdmin ? (
									<>
										<NavDropdown.Item
											className="text-white"
											as={Link}
											to="/admin/dashboard"
										>
											Dashboard
										</NavDropdown.Item>
										<NavDropdown.Item
											className="text-white"
											as={Link}
											to="/admin/users"
										>
											Users
										</NavDropdown.Item>
										<NavDropdown.Item
											className="text-white"
											as={Link}
											to="/admin/products"
										>
											Products
										</NavDropdown.Item>
										<NavDropdown.Divider />
										<NavDropdown.Item
											className="text-white"
											as={Link}
											to="/logout"
										>
											Logout
										</NavDropdown.Item>
									</>
								) : (
									<>
										<NavDropdown.Item
											className="text-white"
											as={Link}
											to={`/profile/${user._id}`}
										>
											Profile
										</NavDropdown.Item>
										<NavDropdown.Item
											className="text-white"
											as={Link}
											to="/products"
										>
											Products
										</NavDropdown.Item>
										<NavDropdown.Item
											className="text-white"
											as={Link}
											to="/cart"
										>
											Cart
										</NavDropdown.Item>
										<NavDropdown.Divider />
										<NavDropdown.Item
											className="text-white"
											as={Link}
											to="/logout"
										>
											Logout
										</NavDropdown.Item>
									</>
								)}
							</NavDropdown>
						</>
					)}
				</Nav>
			</Navbar.Collapse>
		</Navbar>
	);
};

export default NavBar;
