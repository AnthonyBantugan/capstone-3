import { categories } from "../datas";
import CategoryItem from "../components/CategoryItems";
import { Container, Row } from "react-bootstrap";

const Categories = () => {
	return (
		<>
			<Container fluid className="bg-dark p-3">
				<h1 className=" text-center text-light">CATEGORIES</h1>
				<Row
					className="d-flex flex-wrap

				"
				>
					{categories.map((item) => (
						<CategoryItem item={item} key={item.id} />
					))}
				</Row>
			</Container>
		</>
	);
};

export default Categories;
