import { Card, Button, Col } from "react-bootstrap";
import { Link } from "react-router-dom";

const Product = ({ product }) => {
	const { title, description, img, price, _id } = product;
	return (
		<Col className="mb-2">
			<Card
				style={{ minWidth: "18rem" }}
				className="h-100 text-decoration-none"
			>
				<Card.Img variant="top" src={img} className="mb-3" />
				<Card.Body className="d-flex flex-column justify-content-between text-center">
					<Card.Title>{title}</Card.Title>
					<Card.Text className="">{description}</Card.Text>
					<Card.Title className="">
						Price: {price.toLocaleString()}
					</Card.Title>
					<Button as={Link} to={`/products/${_id}`} variant="dark">
						Details
					</Button>
				</Card.Body>
			</Card>
		</Col>
	);
};

export default Product;
