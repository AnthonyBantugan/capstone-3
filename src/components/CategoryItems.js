import { Col, Button } from "react-bootstrap";
import { Link } from "react-router-dom";

const CategoryItems = ({ item }) => {
	return (
		<Col className="m-1  position-relative categoryItemContainer">
			<img src={item.img} alt="category img" className="w-100 " />
			<div className="position-absolute top-50 start-50 translate-middle d-flex flex-column  justify-content-center align-items-center">
				<h1 className="text-center text-dark">{item.title}</h1>
				<Button
					variant="dark"
					as={Link}
					to="/products"
					className="fw-bold"
				>
					SHOP NOW
				</Button>
			</div>
		</Col>
	);
};

export default CategoryItems;
