import { Container, Row, Col } from "react-bootstrap";
import { Link } from "react-router-dom";

const Footer = () => {
	return (
		<Container
			fluid
			className="bg-dark d-flex flex-column flex-md-row justify-content-between align-items-center align-items-md-start py-3 text-light"
		>
			<Row className="d-flex flex-column">
				<Col>
					<h1 className="text-white fw-bold text-center">
						<i className="bi bi-cpu me-2	"></i>Tech Labs
					</h1>
				</Col>
				<Col className="d-flex justify-content-center">
					<div className="socials-icon m-2">
						<a href="www.facebook.com">
							<i className="bi bi-facebook"></i>
						</a>
					</div>

					<div className="socials-icon m-2">
						<a href="www.facebook.com">
							<i className="bi bi-instagram"></i>
						</a>
					</div>
					<div className="socials-icon m-2">
						<a href="www.facebook.com">
							<i className="bi bi-twitter"></i>
						</a>
					</div>
					<div className="socials-icon m-2">
						<a href="www.facebook.com">
							<i className="bi bi-whatsapp"></i>
						</a>
					</div>
				</Col>
			</Row>

			<Row className="center px-5 d-none d-md-block text-center">
				<h3>Useful Links</h3>
				<ul className="d-flex flex-wrap ">
					<li as={Link} to="/">
						Home
					</li>
					<li as={Link} to="/cart">
						Cart
					</li>
					<li as={Link} to="/profile/">
						My Account
					</li>
					<li as={Link} to="">
						Categories
					</li>
					<li as={Link} to="">
						Featured
					</li>
					<li as={Link} to="">
						Wishlist
					</li>
					<li as={Link} to="">
						Order Tracking
					</li>
					<li as={Link} to="">
						Terms
					</li>
				</ul>
			</Row>
			<Row className="d-none d-md-block">
				<h3>Contact Us</h3>
				<div className="contactItem">
					<i className="bi bi-geo-alt"></i>
					<p>Davao City, Philippines</p>
				</div>
				<div className="contactItem">
					<i className="bi bi-telephone"></i>
					<p>(082) 234-9999 / 09981234567</p>
				</div>
				<div className="contactItem">
					<i className="bi bi-envelope"></i>
					<p>contact@store.com</p>
				</div>
				<img
					src="https://i.ibb.co/Qfvn4z6/payment.png"
					alt="payment img"
				/>
			</Row>
		</Container>
	);
};

export default Footer;
