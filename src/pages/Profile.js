import { useEffect, useState } from "react";
import { Container, Row, Col, Accordion } from "react-bootstrap";
import { useParams } from "react-router-dom";
// import UserContext from "../UserContext";

const Profile = () => {
	// const { user } = useContext(UserContext);

	const [name, setName] = useState("");
	const [email, setEmail] = useState("");
	const [address, setAddress] = useState("");
	const [mobile, setMobile] = useState("");
	const [orders, setOrders] = useState([]);

	const { userId } = useParams();

	useEffect(() => {
		fetch(
			`https://still-scrubland-63783.herokuapp.com/users/profile/${userId}`,
			{
				headers: {
					Authorization: `Bearer ${localStorage.getItem("token")}`,
				},
			}
		)
			.then((res) => res.json())
			.then((data) => {
				setName(data.fullName);
				setEmail(data.email);
				setAddress(data.address);
				setMobile(data.mobile);
			});
	}, [userId]);

	useEffect(() => {
		fetch(`https://still-scrubland-63783.herokuapp.com/orders/${userId}`, {
			headers: {
				Authorization: `Bearer ${localStorage.getItem("token")}`,
			},
		})
			.then((res) => res.json())
			.then((data) => {
				setOrders(data);
			});
	}, [userId]);

	return (
		<Container style={{ minHeight: "100vh" }}>
			<Row className="bg-dark text-white mt-5	">
				<Col className="fw-bold text-center p-3">
					<h1>USER PROFILE</h1>
				</Col>
			</Row>
			<Row className="">
				<Col className="p-5 d-flex" style={{ minHeight: "50vh" }}>
					<Col>
						<img
							src="https://www.seekpng.com/png/detail/966-9665317_placeholder-image-person-jpg.png"
							className="w-75"
							alt="profile pic"
						/>
					</Col>
					<Col className="pt-5">
						<Col className="mb-3 fw-bold fs-4 ">
							<span className=" me-3">Full Name:</span>
							{name}
						</Col>
						<Col className="mb-3 fw-bold fs-4">
							<span className=" me-3">Email:</span>
							{email}
						</Col>
						<Col className="mb-3 fw-bold fs-4">
							<span className=" me-3">Address:</span>
							{address}
						</Col>
						<Col className="mb-3 fw-bold fs-4">
							<span className=" me-3">Mobile:</span>
							{mobile}
						</Col>
						<Col className="mb-3 fw-bold fs-4">
							<span className=" me-3">Orders:</span>
						</Col>
						<Col>
							{orders.map((item, i) => (
								<Accordion key={item._id} className="">
									<Accordion.Item eventKey={i}>
										<Accordion.Header>
											<span>Order ID:</span>
											{item._id}
										</Accordion.Header>
										<Accordion.Body>
											<div className="d-flex justify-content-between">
												<span>
													{item.products.length} Items
												</span>
												<span>
													Address: {item.address}
												</span>
												<span>
													Total:{" "}
													{item.total.toLocaleString()}
												</span>
												<span>
													Status: {item.status}
												</span>
											</div>
										</Accordion.Body>
									</Accordion.Item>
								</Accordion>
							))}
						</Col>
					</Col>
				</Col>
			</Row>
		</Container>
	);
};

export default Profile;
