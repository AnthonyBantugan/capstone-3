import { useContext } from "react";
import { Container, Row, Col, Button } from "react-bootstrap";
import UserContext from "../UserContext";
import { Link, Navigate } from "react-router-dom";

const Dashboard = () => {
	const { user } = useContext(UserContext);

	return user === undefined ? (
		<Navigate to="/admin" />
	) : (
		<Container className="">
			<div className="dashboard my-4">
				<Row className="bg-dark text-white text-center p-3">
					<h1>ADMIN DASHBOARD</h1>
				</Row>
				<Row className=" dashboardBtnContainer">
					<Col className="text-center bg-light pt-3">
						<Button
							as={Link}
							to="/admin/users"
							className="dashboardBtn bg-dark px-5 border-0"
						>
							<i className="bi bi-person-rolodex"></i>
							<span className="fw-bold ms-2 fs-5">USERS</span>
						</Button>

						<Button
							as={Link}
							to="/admin/products"
							className="dashboardBtn bg-dark px-5 border-0"
						>
							<i className="bi bi-box-seam"></i>
							<span className="fw-bold ms-2 fs-5">PRODUCTS</span>
						</Button>

						<Button className="dashboardBtn bg-dark px-5 border-0">
							<i className="bi bi-envelope"></i>
							<span className="fw-bold ms-2 fs-5">MESSAGES</span>
						</Button>

						<Button className="dashboardBtn bg-dark px-5 border-0">
							<i className="bi bi-bar-chart"></i>
							<span className="fw-bold ms-2 fs-5">REPORTS</span>
						</Button>
						<Button className="dashboardBtn px-5 bg-dark  border-0">
							<i className="bi bi-tools"></i>
							<span className="fw-bold ms-2 fs-5">
								NOT AVAILABLE
							</span>
						</Button>
						<Button className="dashboardBtn bg-dark px-5 border-0">
							<i className="bi bi-tools"></i>
							<span className="fw-bold ms-2 fs-5">
								NOT AVAILABLE
							</span>
						</Button>
					</Col>
				</Row>
			</div>
		</Container>
	);
};

export default Dashboard;
