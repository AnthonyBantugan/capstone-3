import { useContext, useEffect, useState } from "react";
import { Navigate } from "react-router-dom";
import { Form, Button, Container, Row, Col } from "react-bootstrap";
import Swal from "sweetalert2";
import UserContext from "../UserContext";

const Login = () => {
	const { user, setUser } = useContext(UserContext);

	const [username, setUsername] = useState("");
	const [password, setPassword] = useState("");
	const [isActive, setisActive] = useState(false);

	function loginUser(e) {
		e.preventDefault();
		fetch("https://still-scrubland-63783.herokuapp.com/auth/login", {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
			},
			body: JSON.stringify({
				username: username,
				password: password,
			}),
		})
			.then((res) => res.json())
			.then((data) => {
				if (data) {
					localStorage.setItem("token", data.token);
					setUser(data.user);
					localStorage.setItem("user", JSON.stringify(data.user));

					Swal.fire({
						title: "Login Successful",
						icon: "success",
						text: "Welcome to Tech Labs!",
					});
				} else {
					Swal.fire({
						title: "Authentication Failed",
						icon: "error",
						text: "Check details and try again",
					});
				}
			});
		setUsername("");
		setPassword("");
	}

	useEffect(() => {
		if (username && password !== "") {
			setisActive(true);
		}
	}, [username, password]);

	useEffect(() => {
		const loggedInUser = localStorage.getItem("user");
		if (loggedInUser) {
			const foundUser = JSON.parse(loggedInUser);
			setUser(foundUser);
		}
	}, [setUser]);

	return user !== undefined ? (
		<Navigate to="/" />
	) : (
		<Container
			fluid
			className="form-container d-flex justify-content-center align-items-center"
		>
			<Row>
				<Col>
					<Form
						className="p-3 p-md-5 shadow-lg  bg-light"
						onSubmit={(e) => loginUser(e)}
					>
						<h1 className="text-center fw-bold">Login Account</h1>
						<Form.Group className="mb-1" controlId="loginUsername">
							<Form.Label>Username:</Form.Label>
							<Form.Control
								type="text"
								placeholder="Enter Username"
								value={username}
								onChange={(e) => setUsername(e.target.value)}
								required
							/>
						</Form.Group>

						<Form.Group className="mb-3" controlId="loginPassword">
							<Form.Label>Password:</Form.Label>
							<Form.Control
								type="password"
								placeholder="Enter Password"
								value={password}
								onChange={(e) => setPassword(e.target.value)}
								required
							/>
						</Form.Group>

						{isActive ? (
							<Button
								variant="success"
								type="submit"
								className="w-100"
							>
								Login
							</Button>
						) : (
							<Button
								variant="danger"
								type="submit"
								className="w-100"
								disabled
							>
								Login
							</Button>
						)}
					</Form>
				</Col>
			</Row>
		</Container>
	);
};

export default Login;
