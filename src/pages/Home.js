import Announcement from "../components/Announcement";
import Carousel from "../components/Carousel";
import Categories from "../components/Categories";

const Home = () => {
	return (
		<>
			<Announcement />
			<Carousel />
			<Categories />
		</>
	);
};

export default Home;
