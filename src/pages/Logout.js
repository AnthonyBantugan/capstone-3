import { useContext, useEffect } from "react";
import { Navigate } from "react-router-dom";
import UserContext from "../UserContext";

const Logout = () => {
	const { user, setUser } = useContext(UserContext);

	localStorage.clear();

	useEffect(() => {
		setUser(undefined);
	}, [setUser]);

	return !user.isAdmin ? <Navigate to="/login" /> : <Navigate to="/admin/" />;
};
export default Logout;
