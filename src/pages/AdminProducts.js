import {
	Container,
	Table,
	OverlayTrigger,
	Tooltip,
	Row,
	Col,
	Form,
} from "react-bootstrap";
import { Link, Navigate } from "react-router-dom";
import { useState, useEffect, useContext } from "react";
import UserContext from "../UserContext";

const AdminProducts = () => {
	const { user } = useContext(UserContext);
	const [products, setProducts] = useState([]);
	const [update, setUpdate] = useState(false);

	const handleDelete = (id) => {
		fetch(
			`https://still-scrubland-63783.herokuapp.com/admin/products/${id}`,
			{
				method: "DELETE",
				headers: {
					"Content-Type": "application/json",
					Authorization: `Bearer ${localStorage.getItem("token")}`,
				},
				body: JSON.stringify({
					productId: id,
				}),
			}
		)
			.then((res) => res.json())
			.then((data) => {
				if (update) {
					setUpdate(false);
				} else {
					setUpdate(true);
				}
			});
	};

	const handleInactive = (id) => {
		fetch(
			`https://still-scrubland-63783.herokuapp.com/admin/products/${id}`,
			{
				method: "PUT",
				headers: {
					"Content-Type": "application/json",
					Authorization: `Bearer ${localStorage.getItem("token")}`,
				},
				body: JSON.stringify({
					isActive: false,
				}),
			}
		)
			.then((res) => res.json())
			.then((data) => {
				if (update) {
					setUpdate(false);
				} else {
					setUpdate(true);
				}
			});
	};

	const handleActive = (id) => {
		fetch(
			`https://still-scrubland-63783.herokuapp.com/admin/products/${id}`,
			{
				method: "PUT",
				headers: {
					"Content-Type": "application/json",
					Authorization: `Bearer ${localStorage.getItem("token")}`,
				},
				body: JSON.stringify({
					isActive: true,
				}),
			}
		)
			.then((res) => res.json())
			.then((data) => {
				if (update) {
					setUpdate(false);
				} else {
					setUpdate(true);
				}
			});
	};

	useEffect(() => {
		fetch("https://still-scrubland-63783.herokuapp.com/admin/products", {
			method: "GET",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.getItem("token")}`,
			},
		})
			.then((res) => res.json())
			.then((data) => {
				setProducts(data);
			});
	}, [update]);

	return user.isAdmin === false ? (
		<Navigate to="/admin" />
	) : (
		<Container>
			<Row className="bg-dark text-white my-4">
				<Col
					xs={3}
					className="d-flex justify-content-around align-items-center"
				>
					<span>FILTER:</span>
					<Form.Select
						className="w-50"
						aria-label="Default select example"
					>
						<option>Category</option>
						<option value="1">CPU</option>
						<option value="2">GPU</option>
						<option value="3">Motherboards</option>
						<option value="3">RAM</option>
						<option value="3">PSU</option>
						<option value="3">Keyboards</option>
						<option value="3">Mouse</option>
					</Form.Select>
				</Col>
				<Col className="d-flex justify-content-around align-items-center">
					<h1>PRODUCTS</h1>
					<Link
						className="border rounded-3 p-2 text-white text-decoration-none"
						to="/admin/dashboard"
					>
						Back to Dashboard
					</Link>
				</Col>
			</Row>
			<Table striped bordered hover>
				<thead>
					<tr>
						<th className="tableImg"></th>
						<th>TITLE</th>
						<th>DESCRIPTION</th>
						<th>PRICE</th>
						<th>ACTIVE</th>
						<th>ACTIONS</th>
					</tr>
				</thead>
				<tbody>
					{products.map((item) => (
						<tr key={item._id}>
							<td className="tableImg">
								<img
									className="w-100"
									src={item.img}
									alt="product img"
								/>
							</td>
							<td className="tableTitle fw-bold">{item.title}</td>
							<td className="tableDesc">{item.description}</td>
							<td className="tablePrice">
								P {item.price.toLocaleString()}
							</td>
							<td className="tableActive">{`${item.isActive}`}</td>
							<td className="tableActions ">
								<div className="tableActions mt-3 ">
									<OverlayTrigger
										overlay={
											<Tooltip>
												<strong>UPDATE</strong>.
											</Tooltip>
										}
									>
										<Link
											to={`/admin/products/${item._id}`}
										>
											<i className="ms-4 bi bi-pencil-square text-dark"></i>
										</Link>
									</OverlayTrigger>

									{!item.isActive ? (
										<OverlayTrigger
											overlay={
												<Tooltip>
													<strong>SET ACTIVE</strong>
												</Tooltip>
											}
										>
											<i
												className="bi bi-unlock p-2 mx-3  "
												onClick={() =>
													handleActive(item._id)
												}
											></i>
										</OverlayTrigger>
									) : (
										<OverlayTrigger
											overlay={
												<Tooltip>
													<strong>
														SET INACTIVE
													</strong>
												</Tooltip>
											}
										>
											<i
												className="bi bi-lock mx-4"
												onClick={() =>
													handleInactive(item._id)
												}
											></i>
										</OverlayTrigger>
									)}

									<OverlayTrigger
										overlay={
											<Tooltip>
												<strong>DELETE</strong>
											</Tooltip>
										}
									>
										<i
											className="bi bi-trash bg-danger text-light p-2 rounded-3"
											onClick={() =>
												handleDelete(item._id)
											}
										></i>
									</OverlayTrigger>
								</div>
							</td>
						</tr>
					))}
				</tbody>
			</Table>
		</Container>
	);
};

export default AdminProducts;
