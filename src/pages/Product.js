import { Container, Row, Col, Button } from "react-bootstrap";
import { useContext, useEffect, useState } from "react";
import { useParams, Link } from "react-router-dom";
import UserContext from "../UserContext";
import Swal from "sweetalert2";

const Product = () => {
	const { user, setUser, cart, setBadge } = useContext(UserContext);

	const [product, setProduct] = useState();
	const [title, setTitle] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
	const [img, setImg] = useState("");
	const [quantity, setQuantity] = useState(1);

	const { productId } = useParams();

	const handleQuantity = (type) => {
		if (type === "decrease") {
			quantity > 1 && setQuantity(quantity - 1);
		} else {
			setQuantity(quantity + 1);
		}
	};

	const addToCart = (data) => {
		if (user.isAdmin) {
			Swal.fire({
				icon: "error",
				title: "Oops...",
				text: "Admin accounts cannot add to cart.",
			});
		}
		data.quantity = quantity;
		if (!cart.includes(data)) {
			cart.push(data);
			localStorage.setItem("cart", JSON.stringify(cart));
			setBadge(cart.length);
			Swal.fire({
				title: "Added to Cart",
				icon: "success",
			});
		}
	};

	useEffect(() => {
		fetch(
			`https://still-scrubland-63783.herokuapp.com/products/${productId}`
		)
			.then((res) => res.json())
			.then((data) => {
				setTitle(data.title);
				setDescription(data.description);
				setPrice(data.price);
				setImg(data.img);
				setProduct(data);
			});
	}, [productId]);

	useEffect(() => {
		const loggedInUser = localStorage.getItem("user");
		if (loggedInUser) {
			const foundUser = JSON.parse(loggedInUser);
			setUser(foundUser);
		}
	}, [setUser]);

	return (
		<Container className="d-flex " style={{ height: "90vh" }}>
			<Row className="d-flex flex-column flex-md-row">
				<Col className=" imgContainer d-flex">
					<img src={img} alt="product img" />
				</Col>
				<Col className="infoContainer d-flex flex-column my-auto text-center">
					<h1>{title}</h1>
					<p>{description}</p>
					<div>P {price.toLocaleString()}</div>
					<Col>
						{user !== undefined && (
							<Col className=" addQuantity  ">
								<i
									className="bi bi-dash-circle"
									onClick={() => handleQuantity("decrease")}
								></i>
								<span className="px-4">{quantity}</span>
								<i
									className="bi bi-plus-circle"
									onClick={() => handleQuantity("increase")}
								></i>
							</Col>
						)}
						<Col className="mb-3">
							{user !== undefined ? (
								<>
									<Col className="d-flex flex-column align-items-center">
										{!user.isAdmin && (
											<Button
												className="btn-dark   fw-bold w-50 my-2"
												onClick={() =>
													addToCart(product)
												}
											>
												ADD TO CART
											</Button>
										)}
										<Button
											as={Link}
											to="/cart"
											className="btn-dark w-50 fw-bold"
										>
											CHECKOUT
										</Button>
									</Col>
									<Col
										as={Link}
										to="/products"
										className="cursorPointer fs-5 link-dark text-decoration-none "
									>
										<i className="bi bi-arrow-left-circle pe-3"></i>
										Continue Shopping
									</Col>
								</>
							) : (
								<Link
									className="btn btn-dark fw-bold"
									to="/login"
								>
									LOGIN to Continue
								</Link>
							)}
						</Col>
					</Col>
				</Col>
			</Row>
		</Container>
	);
};

export default Product;
