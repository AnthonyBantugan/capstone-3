import {
	Container,
	Table,
	OverlayTrigger,
	Tooltip,
	Row,
	Col,
	Form,
} from "react-bootstrap";
import { Link, Navigate } from "react-router-dom";
import { useState, useEffect, useContext } from "react";
import UserContext from "../UserContext";

const AdminUsers = () => {
	const { user } = useContext(UserContext);
	const [users, setUsers] = useState([]);
	const [update, setUpdate] = useState(false);
	const [mounted, setMounted] = useState(false);

	const handleDelete = (id) => {
		fetch(`https://still-scrubland-63783.herokuapp.com/admin/users`, {
			method: "DELETE",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.getItem("token")}`,
			},
			body: JSON.stringify({
				userId: id,
			}),
		})
			.then((res) => res.json())
			.then((data) => {
				if (update) {
					setUpdate(false);
				} else {
					setUpdate(true);
				}
			});
	};

	const handleSetUser = (id) => {
		fetch(`https://still-scrubland-63783.herokuapp.com/admin/users/${id}`, {
			method: "PUT",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.getItem("token")}`,
			},
			body: JSON.stringify({
				isAdmin: false,
			}),
		})
			.then((res) => res.json())
			.then((data) => {
				if (update) {
					setUpdate(false);
				} else {
					setUpdate(true);
				}
			});
	};

	const handleSetAdmin = (id) => {
		fetch(`https://still-scrubland-63783.herokuapp.com/admin/users/${id}`, {
			method: "PUT",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.getItem("token")}`,
			},
			body: JSON.stringify({
				isAdmin: true,
			}),
		})
			.then((res) => res.json())
			.then((data) => {
				if (update) {
					setUpdate(false);
				} else {
					setUpdate(true);
				}
			});
	};

	// useEffects

	useEffect(() => {
		let isMounted = false;

		fetch("https://still-scrubland-63783.herokuapp.com/admin/users", {
			method: "GET",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.getItem("token")}`,
			},
		})
			.then((res) => res.json())
			.then((data) => {
				setUsers(data);
				if (!isMounted) {
					setMounted(true);
				}
			});

		return () => {
			isMounted = true;
		};
	}, [update, mounted]);

	return user === undefined ? (
		<Navigate to="/admin" />
	) : (
		<Container className="adminUserContainer">
			<Row className="bg-dark text-white my-4">
				<Col
					xs={3}
					className="d-flex justify-content-around align-items-center"
				>
					<span>FILTER:</span>
					<Form.Select
						className="w-50"
						aria-label="Default select example"
					>
						<option value="all">All</option>
						<option value="admin">Admin</option>
						<option value="users">Users</option>
					</Form.Select>
				</Col>
				<Col className=" d-flex justify-content-around align-items-center">
					<h1>USERS</h1>
					<Link
						className="border rounded-3 p-2 text-white text-decoration-none"
						to="/admin/dashboard"
					>
						Back to Dashboard
					</Link>
				</Col>
			</Row>
			<Table striped bordered hover>
				<thead>
					<tr>
						<th>ID</th>
						<th>FULLNAME</th>
						<th>ADDRESS</th>
						<th>EMAIL</th>
						<th>MOBILE</th>
						<th>isADMIN</th>
						<th>ACTIONS</th>
					</tr>
				</thead>

				<tbody>
					{users.map((user) => (
						<tr key={user._id}>
							<td className="">{user._id}</td>
							<td className=" fw-bold">{user.fullName}</td>
							<td className="">{user.address}</td>
							<td className="">{user.email}</td>
							<td className="">{user.mobile}</td>
							<td className="">{`${user.isAdmin}`}</td>
							<td className=" ">
								<div className="tableActions mt-3 ">
									<OverlayTrigger
										overlay={
											<Tooltip>
												<strong>ORDERS</strong>.
											</Tooltip>
										}
									>
										<Link to={`/admin/users/${user._id}`}>
											<i className="ms-4 bi bi-pencil-square text-dark"></i>
										</Link>
									</OverlayTrigger>

									{!user.isAdmin ? (
										<OverlayTrigger
											overlay={
												<Tooltip>
													<strong>
														SET TO ADMIN?
													</strong>
												</Tooltip>
											}
										>
											<i
												className="bi bi-suit-spade-fill p-2 mx-3 cursorPointer "
												onClick={() =>
													handleSetAdmin(user._id)
												}
											></i>
										</OverlayTrigger>
									) : (
										<OverlayTrigger
											overlay={
												<Tooltip>
													<strong>
														SET TO USER?
													</strong>
												</Tooltip>
											}
										>
											<i
												className="bi bi-suit-club-fill mx-4 cursorPointer"
												onClick={() =>
													handleSetUser(user._id)
												}
											></i>
										</OverlayTrigger>
									)}

									{user._id !==
										"6230e8841b370e6a85db5041" && (
										<OverlayTrigger
											overlay={
												<Tooltip>
													<strong>REMOVE</strong>
												</Tooltip>
											}
										>
											<i
												className="bi bi-trash bg-danger text-light p-1  rounded-3 cursorPointer"
												onClick={() =>
													handleDelete(user._id)
												}
											></i>
										</OverlayTrigger>
									)}
								</div>
							</td>
						</tr>
					))}
				</tbody>
			</Table>
		</Container>
	);
};

export default AdminUsers;
