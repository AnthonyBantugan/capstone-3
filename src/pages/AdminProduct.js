import { Container, Row, Col, Button, Form } from "react-bootstrap";
import { useEffect, useState } from "react";
import { useParams, Link, Navigate } from "react-router-dom";
import Swal from "sweetalert2";

const AdminProduct = () => {
	const [user, setUser] = useState({});

	const [title, setTitle] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
	const [img, setImg] = useState("");
	const [updated, setUpdated] = useState(false);

	const { productId } = useParams();

	const updateProduct = (e) => {
		e.preventDefault();
		fetch(
			`https://still-scrubland-63783.herokuapp.com/admin/products/${productId}/update`,
			{
				method: "PUT",
				headers: {
					"Content-Type": "application/json",
					Authorization: `Bearer ${localStorage.getItem("token")}`,
				},
				body: JSON.stringify({
					title: title,
					description: description,
					price: price,
				}),
			}
		)
			.then((res) => res.json())
			.then((data) => {
				if (data) {
					setTitle(data.title);
					setDescription(data.description);
					setPrice(data.price);
					setUpdated(true);
					Swal.fire({
						title: "Update successful",
						icon: "success",
						text: "Successfully updated product details",
					});
				} else {
					Swal.fire({
						title: "Something went wrong",
						icon: "error",
						text: "Please try again",
					});
				}
			});
	};

	useEffect(() => {
		fetch(
			`https://still-scrubland-63783.herokuapp.com/admin/products/${productId}`,
			{
				method: "GET",
				headers: {
					"Content-Type": "application/json",
					Authorization: `Bearer ${localStorage.getItem("token")}`,
				},
			}
		)
			.then((res) => res.json())
			.then((data) => {
				setTitle(data.title);
				setDescription(data.description);
				setPrice(data.price);
				setImg(data.img);
			});
	}, [updated, productId]);

	useEffect(() => {
		const loggedInUser = localStorage.getItem("user");
		if (loggedInUser) {
			const foundUser = JSON.parse(loggedInUser);
			setUser(foundUser);
		}
	}, []);

	return user === undefined ? (
		<Navigate to="/admin" />
	) : (
		<Container
			className="d-flex justify-content-between"
			style={{ minHeight: "80vh" }}
		>
			<Row className="d-flex">
				<Col className=" d-flex flex-column justify-content-center text-center">
					<div className="mb-5">
						<div>
							<img src={img} className="w-50" alt="" />
						</div>
					</div>
					<div>
						<div>
							<h1>{title}</h1>
							<p className="text-muted"> {description}</p>
							<span className="fw-bold fs-5">
								P {price.toLocaleString()}
							</span>
						</div>
					</div>
				</Col>
				<Col className=" d-flex justify-content-center align-items-center  bg-light">
					<Form
						className="shadow-lg p-4"
						onSubmit={(e) => updateProduct(e)}
					>
						<span className="fs-3 fw-bold">
							UPDATE PRODUCT DETAILS
						</span>
						<Form.Group className="mb-3" controlId="productTitle">
							<Form.Label>Title:</Form.Label>
							<Form.Control
								type="text"
								placeholder={title}
								onChange={(e) => setTitle(e.target.value)}
							/>
						</Form.Group>

						<Form.Group className="mb-3" controlId="productTitle">
							<Form.Label>Price:</Form.Label>
							<Form.Control
								type="number"
								placeholder={price}
								onChange={(e) => setPrice(e.target.value)}
							/>
						</Form.Group>

						<Form.Group className="mb-3" controlId="productDesc">
							<Form.Label>Description:</Form.Label>
							<Form.Control
								as="textarea"
								placeholder={description}
								aria-label="With textarea"
								onChange={(e) => setDescription(e.target.value)}
							/>
						</Form.Group>
						<div className="d-flex justify-content-around fw-bold">
							<Button variant="dark" type="submit">
								Update
							</Button>
							<Button
								variant="dark"
								as={Link}
								to="/admin/products"
							>
								Back to Products
							</Button>
						</div>
					</Form>
				</Col>
			</Row>
		</Container>
	);
};

export default AdminProduct;
