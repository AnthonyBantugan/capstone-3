import { useState, useEffect, useContext } from "react";
import { Container, Row, Col, FormControl, Button } from "react-bootstrap";
import { Link, Navigate, useNavigate } from "react-router-dom";
import UserContext from "../UserContext";
import Swal from "sweetalert2";
const Cart = () => {
	const { user, setUser, cart, setCart, setBadge } = useContext(UserContext);
	const navigate = useNavigate();

	const [subtotal, setSubtotal] = useState(0);
	const [shipping] = useState(0);
	const [discount] = useState(0);
	const [total, setTotal] = useState(0);

	// Decrement Quantity
	const minusQuantity = (id) => {
		setCart((cart) =>
			cart.map((item) =>
				id === item._id
					? {
							...item,
							quantity:
								item.quantity - (item.quantity > 1 ? 1 : 0),
					  }
					: item
			)
		);
		setSubtotal(
			cart
				.map((item) => item.quantity * item.price)
				.reduce((prev, curr) => prev + curr, 0)
		);
	};
	// Increment Quantity
	const addQuantity = (id) => {
		setCart((cart) =>
			cart.map((item) =>
				id === item._id
					? {
							...item,
							quantity:
								item.quantity + (item.quantity < 10 ? 1 : 0),
					  }
					: item
			)
		);

		setSubtotal(
			cart
				.map((item) => item.quantity * item.price)
				.reduce((prev, curr) => prev + curr, 0)
		);
	};
	// Delete Cart Item
	const removeItem = (id) => {
		const cartData = JSON.parse(localStorage.getItem("cart")).filter(
			(item) => item._id !== id
		);
		setCart(cartData);
		localStorage.setItem("cart", JSON.stringify(cartData));

		setSubtotal(
			cart
				.map((item) => item.quantity * item.price)
				.reduce((prev, curr) => prev + curr, 0)
		);
	};
	// Checkout
	const handleOrder = () => {
		fetch("https://still-scrubland-63783.herokuapp.com/orders", {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.getItem("token")}`,
			},
			body: JSON.stringify({
				userId: user._id,
				products: cart,
				address: user.address,
				total: total,
			}),
		})
			.then((res) => res.json())
			.then((data) => {
				if (data) {
					localStorage.setItem("order", JSON.stringify(data));
					setCart([]);
					setBadge(0);
					Swal.fire({
						title: "Order Successful",
						icon: "success",
					});

					navigate(`/orders/${user._id}`);
				} else {
					Swal.fire({
						title: "Something went wrong",
						icon: "error",
						text: "Please try again",
					});
				}
			});
	};

	// [useEffects]
	// Get localStorage "cart"
	useEffect(() => {
		const cartItems = localStorage.getItem("cart");
		if (cartItems) {
			const carts = JSON.parse(cartItems);
			setCart(carts);
		}
	}, [setCart]);
	// Get localStorage "user"
	useEffect(() => {
		const loggedInUser = localStorage.getItem("user");
		if (loggedInUser) {
			const foundUser = JSON.parse(loggedInUser);
			setUser(foundUser);
		}
	}, [setUser]);
	// Subtotal
	useEffect(() => {
		if (cart.length !== 0) {
			const subTotal = cart
				.map((item) => item.quantity * item.price)
				.reduce((prev, curr) => prev + curr);
			setSubtotal(subTotal);
			setTotal(subTotal - shipping - discount);
		} else {
			setSubtotal(0);
			setTotal(0);
		}

		setBadge(cart.length);
	}, [cart, discount, setBadge, shipping]);

	return user === null ? (
		<Navigate to="/login" />
	) : (
		<Container fluid md="true">
			<Row className=" shadow-lg my-5 my-md-0 rounded-3 ">
				<Col lg={9} className=" cartContainer p-0">
					<Col className="d-flex justify-content-around  align-items-center py-3 ">
						<span className="fs-1 fw-bold">SHOPPING CART</span>
						<span className="fw-bold fs-5 d-none d-md-block">
							{cart.length} Items
						</span>
					</Col>
					{cart.length === 0 ? (
						<>
							<Col className="text-center fw-bold fs-3 flex-column d-flex">
								<span className="text-center">
									Nothing found
								</span>
								<Col
									as={Link}
									to="/products"
									className="cursorPointer  p-3  fs-5 mb-3 text-dark text-decoration-none "
								>
									<i className="bi bi-arrow-left-circle pe-3 "></i>
									Go to Shop
								</Col>
							</Col>
						</>
					) : (
						cart.map((item) => (
							<Row
								key={item._id}
								className="d-flex border-top border-bottom p-3 p-md-0"
							>
								<Col className="d-flex" xs={12} lg={8}>
									<img
										src={item.img}
										className="img-fluid w-25"
										alt="product img"
									/>
									<Col
										lg={8}
										className="d-flex flex-column justify-content-center ps-3"
									>
										<span className="cartItemTitle fw-bold  ">
											{item.title}
										</span>
										<span className="cartItemPrice ">
											P {item.price.toLocaleString()}
										</span>
									</Col>
								</Col>
								<Col
									lg={2}
									className="cartItemAdd d-flex justify-content-around justify-content-md-center 
									align-items-center  fw-bold mt-3 mt-md-0 mb-md-2"
								>
									<div className="cartQty">
										<i
											className="bi bi-dash-circle"
											onClick={() =>
												minusQuantity(item._id)
											}
										></i>
										<span className="px-4 ">
											{item.quantity}
										</span>
										<i
											className="bi bi-plus-circle"
											onClick={() =>
												addQuantity(item._id)
											}
										></i>
									</div>
									<div className="subtotal fs-5 mx-md-5">
										P{" "}
										{(
											item.price * item.quantity
										).toLocaleString()}
									</div>
									<div className="cartDelete px-2  rounded text-dark">
										<i
											className="bi bi-trash3"
											onClick={() => removeItem(item._id)}
										></i>
									</div>
								</Col>
							</Row>
						))
					)}
				</Col>
				<Col className="bg-light">
					<Col className="p-3 fs-1 fw-bold border-bottom">
						<span>Summary</span>
					</Col>
					<Col className="my-2">
						<div className="d-flex justify-content-between p-3 text-muted fw-bold fs-5 ">
							<span className="">Subtotal:</span>
							<span>P {subtotal.toLocaleString()}</span>
						</div>
						<div className="px-3 my-3">
							<FormControl
								placeholder="Enter coupon"
								aria-label="Username"
								aria-describedby="basic-addon1"
							/>
						</div>
						<div className="d-flex justify-content-between px-3 my-3 text-muted  ">
							<span>Shipping:</span>
							<span>P 0</span>
						</div>
						<div className="d-flex justify-content-between px-3 my-3 text-muted  ">
							<span>Discount:</span>
							<span>P 0</span>
						</div>
						<div className="d-flex justify-content-between p-3 fw-bold fs-4 border-top border-bottom ">
							<span>Total:</span>
							<span>P {total.toLocaleString()}</span>
						</div>
					</Col>
					<Col className="text-center d-flex mt-3 flex-column">
						{cart.length !== 0 ? (
							<Button
								className="btn-dark fw-bold  py-2 w-50 mx-auto "
								onClick={() => handleOrder()}
							>
								ORDER
							</Button>
						) : (
							<Button
								className="btn-dark fw-bold py-2"
								onClick={() => handleOrder()}
								disabled
							>
								ORDER
							</Button>
						)}
						<Col
							as={Link}
							to="/products"
							className="cursorPointer mt-3 fs-5 link-dark text-decoration-none mb-3"
						>
							<i className="bi bi-arrow-left-circle pe-3"></i>
							Continue shopping
						</Col>
					</Col>
				</Col>
			</Row>
		</Container>
	);
};

export default Cart;
