import Product from "../components/Product";
import { useEffect, useState } from "react";
import { Row, Container, Col, Form } from "react-bootstrap";
import Announcement from "../components/Announcement";

const Products = () => {
	const [products, setProducts] = useState([]);

	useEffect(() => {
		fetch("https://still-scrubland-63783.herokuapp.com/products")
			.then((res) => res.json())
			.then((data) => {
				setProducts(data);
			});
	}, []);

	return (
		<>
			<Announcement />
			<Container fluid className="">
				<Row className="bg-dark text-white mb-3">
					<Col
						xs={3}
						className="d-flex justify-content-between align-items-center"
					>
						<span>FILTER:</span>
						<Form.Select
							className="ms-2"
							aria-label="Default select example"
						>
							<option disabled>Category</option>
							<option value="1">CPU</option>
							<option value="2">GPU</option>
							<option value="3">Motherboards</option>
							<option value="3">RAM</option>
							<option value="3">PSU</option>
							<option value="3">Keyboards</option>
							<option value="3">Mouse</option>
						</Form.Select>
					</Col>
					<Col className="text-center ms-5 p-0">
						<h1>AVAILABLE PRODUCTS</h1>
					</Col>
				</Row>
				<Row className=" bg-light">
					{products.slice(0, 30).map((product) => (
						<Product product={product} key={product._id} />
					))}
				</Row>
			</Container>
		</>
	);
};

export default Products;
