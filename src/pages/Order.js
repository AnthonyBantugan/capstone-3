import { Container, Col } from "react-bootstrap";
import { Link, Navigate, useParams } from "react-router-dom";
import { useEffect, useState } from "react";
// import UserContext from "../UserContext";

const Order = () => {
	// const { user, setUser } = useContext(UserContext);
	const { userId } = useParams();

	const [user, setUser] = useState({});
	const [id, setId] = useState("");
	const [address, setAddress] = useState("");
	const [orderId, setOrderId] = useState("");
	const [date, setDate] = useState("");

	useEffect(() => {
		const loggedInUser = localStorage.getItem("user");
		if (loggedInUser) {
			const foundUser = JSON.parse(loggedInUser);
			setUser(foundUser);
		}
	}, [setUser]);

	useEffect(() => {
		const order = JSON.parse(localStorage.getItem("order"));
		setId(userId);
		setAddress(order.address);
		setOrderId(order._id);
		setDate(order.createdAt);
	}, [userId]);

	return user === undefined ? (
		<Navigate to="/login" />
	) : (
		<Container
			style={{ height: "100vh" }}
			className="d-flex justify-content-center align-items-center"
		>
			<div className="orderContainer shadow-lg rounded-3 bg-light">
				<Col className="t">
					<Col className="bg-dark text-white fw-bold fs-1 rounded-top p-3 text-center">
						ORDER
					</Col>
					<Col className="d-flex border-top border-bottom p-5 ">
						<Col className="flex-column d-flex">
							<div>
								<span className="me-2 fw-bold">User ID:</span>
								{id}
							</div>

							<div>
								<span className="me-2 fw-bold">Address:</span>
								{address}
							</div>
						</Col>

						<Col className="d-flex flex-column ">
							<div>
								<span className="me-2 fw-bold">Order ID:</span>{" "}
								{orderId}
							</div>

							<div>
								<span className="me-2 fw-bold">
									Purchase Date:
								</span>{" "}
								{date}
							</div>
						</Col>
					</Col>
					<Col className="d-flex flex-column align-items-center">
						<i className="bi bi-clipboard-check "></i>
						<span className="text-muted">
							Thank you for your purchase.
						</span>
						<p className="text-muted">
							Visit your <Link to="/">account & billing</Link>{" "}
							page to manage your invoices, products, and
							services.
						</p>
						<p className="text-muted">
							If you have any questions, please contact us at{" "}
							<Link to="/">billing@techlabs.com</Link>
						</p>
					</Col>
				</Col>
			</div>
		</Container>
	);
};

export default Order;
