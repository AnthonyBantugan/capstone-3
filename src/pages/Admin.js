import { useContext, useEffect, useState } from "react";
import { Navigate } from "react-router-dom";
import { Form, Button, Container } from "react-bootstrap";
import Swal from "sweetalert2";
import UserContext from "../UserContext";

const Admin = () => {
	const { user, setUser } = useContext(UserContext);

	const [username, setUsername] = useState("");
	const [password, setPassword] = useState("");
	const [isActive, setisActive] = useState(false);

	function loginAdmin(e) {
		e.preventDefault();
		fetch("https://still-scrubland-63783.herokuapp.com/admin", {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
			},
			body: JSON.stringify({
				username: username,
				password: password,
			}),
		})
			.then((res) => res.json())
			.then((data) => {
				if (data.user?.isAdmin === true) {
					localStorage.setItem("token", data.token);
					setUser(data.user);
					localStorage.setItem("user", JSON.stringify(data.user));

					Swal.fire({
						title: "Login Successful",
						icon: "success",
						text: "Welcome to Tech Labs!",
					});
				} else {
					Swal.fire({
						title: "Authentication Failed",
						icon: "error",
						text: "Check details and try again",
					});
				}
			});
		setUsername("");
		setPassword("");
		setisActive(false);
	}

	useEffect(() => {
		if (username && password !== "") {
			setisActive(true);
		}
	}, [username, password]);

	return user !== undefined ? (
		<Navigate to="/admin/dashboard" />
	) : (
		<div className="form-container">
			<Container>
				<div className="form-wrapper">
					<h1 className="fw-bold py-3">Login Admin Account</h1>
					<Form
						className="w-25 shadow-lg p-4 bg-light"
						onSubmit={(e) => loginAdmin(e)}
					>
						<Form.Group className="mb-1" controlId="loginUsername">
							<Form.Label>Username:</Form.Label>
							<Form.Control
								type="text"
								placeholder="Enter Username"
								value={username}
								onChange={(e) => setUsername(e.target.value)}
								required
							/>
						</Form.Group>

						<Form.Group className="mb-3" controlId="loginPassword">
							<Form.Label>Password:</Form.Label>
							<Form.Control
								type="password"
								placeholder="Enter Password"
								value={password}
								onChange={(e) => setPassword(e.target.value)}
								required
							/>
						</Form.Group>

						{isActive ? (
							<Button
								variant="success"
								type="submit"
								className="w-100"
							>
								Login
							</Button>
						) : (
							<Button
								variant="danger"
								type="submit"
								className="w-100"
								disabled
							>
								Login
							</Button>
						)}
					</Form>
				</div>
			</Container>
		</div>
	);
};

export default Admin;
