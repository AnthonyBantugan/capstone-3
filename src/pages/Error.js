import React from "react";
import { useContext } from "react";
import { Container, Row, Col } from "react-bootstrap";
import { Link } from "react-router-dom";
import UserContext from "../UserContext";

const Error = () => {
	const { user } = useContext(UserContext);
	return (
		<Container style={{ minHeight: "70vh" }}>
			<Row className="text-center mt-5 d-flex flex-column">
				<Col className="text-center">
					<img
						src="https://www.pngitem.com/pimgs/m/11-111981_sad-face-icon-png-transparent-png.png"
						className="w-25"
						alt="sad face"
					/>
					<h1>404 Page Not Found</h1>
				</Col>
				<Col className="d-flex justify-content-center">
					{user !== undefined ? (
						<div className="me-5 text-decoration-none">
							Back to{" "}
							<Link className="" to="/">
								Home
							</Link>
						</div>
					) : (
						<div className="me-5 text-decoration-none">
							<Link className="" to="/login">
								Login
							</Link>{" "}
						</div>
					)}
					{user === undefined && (
						<div>
							<Link className="" to="/register">
								Create an Account
							</Link>
						</div>
					)}
				</Col>
			</Row>
		</Container>
	);
};

export default Error;
