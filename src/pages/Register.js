import { useState, useEffect, useContext } from "react";
import { Navigate } from "react-router-dom";
import { Form, Button, Container, Row, Col } from "react-bootstrap";
import Swal from "sweetalert2";
import UserContext from "../UserContext";

const Register = () => {
	const { user, setUser } = useContext(UserContext);

	const [fullName, setFullName] = useState("");
	const [address, setAddress] = useState("");
	const [email, setEmail] = useState("");
	const [username, setUsername] = useState("");
	const [password, setPassword] = useState("");
	const [mobile, setMobile] = useState("");
	const [confirmPassword, setConfirmPassword] = useState("");
	const [isActive, setisActive] = useState(false);

	function registerUser(e) {
		e.preventDefault();

		fetch("https://still-scrubland-63783.herokuapp.com/auth/register", {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
			},
			body: JSON.stringify({
				fullName: fullName,
				address: address,
				mobile: mobile,
				email: email,
				username: username,
				password: password,
				confirmPassword: confirmPassword,
			}),
		})
			.then((res) => res.json())
			.then((data) => {
				if (data) {
					setFullName("");
					setAddress("");
					setMobile("");
					setEmail("");
					setUsername("");
					setPassword("");
					setConfirmPassword("");

					Swal.fire({
						title: "Registration successful",
						icon: "success",
						text: "Welcome to Tech Labs",
					});

					<Navigate to="/login" />;
				} else {
					Swal.fire({
						title: "Something went wrong",
						icon: "error",
						text: "Please try again",
					});
				}
			});
	}

	useEffect(() => {
		if (
			email &&
			fullName &&
			address &&
			mobile &&
			username &&
			confirmPassword !== ""
		) {
			setisActive(true);
		} else {
			setisActive(false);
		}
	}, [email, password, fullName, address, mobile, username, confirmPassword]);

	useEffect(() => {
		const loggedInUser = localStorage.getItem("user");
		if (loggedInUser) {
			const foundUser = JSON.parse(loggedInUser);
			setUser(foundUser);
		}
	}, [setUser]);

	return user !== undefined ? (
		<Navigate to="/" />
	) : (
		<>
			<Container
				fluid
				className="registerContainer d-flex justify-content-center align-items-center "
			>
				<Row>
					<Col>
						<Form
							className="p-3 bg-light rounded-3  shadow-lg"
							onSubmit={(e) => registerUser(e)}
						>
							<h1 className="text-center p-2">
								Create an Account
							</h1>
							<Row>
								<Form.Group
									as={Col}
									className="mb-1"
									controlId="fullName"
								>
									<Form.Label>Full Name:</Form.Label>
									<Form.Control
										type="text"
										value={fullName}
										onChange={(e) =>
											setFullName(e.target.value)
										}
										required
										placeholder="Enter Full Name"
									/>
								</Form.Group>
								<Form.Group
									as={Col}
									className="mb-1"
									controlId="email"
								>
									<Form.Label>Email:</Form.Label>
									<Form.Control
										type="text"
										value={email}
										onChange={(e) =>
											setEmail(e.target.value)
										}
										required
										placeholder="Enter Email"
									/>
								</Form.Group>
							</Row>
							<Form.Group
								as={Col}
								className="mb-1"
								controlId="address"
							>
								<Form.Label>Address:</Form.Label>
								<Form.Control
									type="text"
									value={address}
									onChange={(e) => setAddress(e.target.value)}
									required
									placeholder="Enter Address"
								/>
							</Form.Group>
							<Row>
								<Form.Group
									as={Col}
									className="mb-1"
									controlId="username"
								>
									<Form.Label>Username:</Form.Label>
									<Form.Control
										type="text"
										value={username}
										onChange={(e) =>
											setUsername(e.target.value)
										}
										required
										placeholder="Enter Username"
									/>
								</Form.Group>
								<Form.Group
									className=""
									controlId="mobile"
									as={Col}
								>
									<Form.Label>Mobile No.</Form.Label>
									<Form.Control
										type="Number"
										value={mobile}
										onChange={(e) =>
											setMobile(e.target.value)
										}
										required
										placeholder="Mobile No."
									/>
								</Form.Group>
							</Row>

							<Row className="mb-2">
								<Form.Group
									as={Col}
									className="mb-1"
									controlId="password"
								>
									<Form.Label>Password:</Form.Label>
									<Form.Control
										type="password"
										value={password}
										onChange={(e) =>
											setPassword(e.target.value)
										}
										required
										placeholder="Enter Password"
									/>
								</Form.Group>

								<Form.Group
									className=""
									controlId="confirmPassword"
									as={Col}
								>
									<Form.Label>Confirm Password</Form.Label>
									<Form.Control
										type="password"
										value={confirmPassword}
										onChange={(e) =>
											setConfirmPassword(e.target.value)
										}
										required
										placeholder="Confirm Password"
									/>
								</Form.Group>
								<Form.Text className="text-muted">
									We'll never share your details with anyone
									else.
								</Form.Text>
							</Row>

							{isActive ? (
								<Button
									variant="success"
									type="submit"
									className="w-100"
								>
									Create
								</Button>
							) : (
								<Button
									variant="danger"
									type="submit"
									className="w-100"
									disabled
								>
									Create
								</Button>
							)}
						</Form>
					</Col>
				</Row>
			</Container>
		</>
	);
};

export default Register;
