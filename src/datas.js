export const sliderItems = [
	{
		id: 1,
		img: "https://images.pexels.com/photos/852860/pexels-photo-852860.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260",
		title: "SUMMER SALE",
		desc: "DON'T COMPROMISE ON STYLE! GET FLAT 30% OFF FOR NEW ARRIVALS.",
		bg: "E8EFDB",
	},
	{
		id: 2,
		img: "https://images.unsplash.com/photo-1590159983013-d4ff5fc71c1d?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80",
		title: "AUTUMN COLLECTION",
		desc: "DON'T COMPROMISE ON STYLE! GET FLAT 30% OFF FOR NEW ARRIVALS.",
		bg: "fcf1ed",
	},
	{
		id: 3,
		img: "https://images.pexels.com/photos/4602025/pexels-photo-4602025.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260",
		title: "POPULAR",
		desc: "DON'T COMPROMISE ON STYLE! GET FLAT 30% OFF FOR NEW ARRIVALS.",
		bg: "F8B8AE",
	},
];

export const categories = [
	{
		id: 1,
		img: "https://www.pcgamesn.com/wp-content/uploads/2017/09/cpu-upgrade.jpg",
		title: "CPU",
		category: "cpu",
	},
	{
		id: 2,
		img: "https://www.pcgamesn.com/wp-content/uploads/2020/06/rog_strix_z490_e_gaming_motherboard.jpg",
		title: "MOTHERBOARDS",
		category: "motherboards",
	},
	{
		id: 3,
		img: "https://www.hardwaretimes.com/wp-content/uploads/2020/01/14-487-488-V01.jpg",
		title: "GRAPHICS CARDS",
		category: "gpu",
	},
	{
		id: 4,
		img: "https://m.media-amazon.com/images/I/41p5RU+yaAL._AC_.jpg",
		title: "RAM",
		category: "ram",
	},
	{
		id: 5,
		img: "https://thermaltake.azureedge.net/pub/media/catalog/product/cache/6bf0ed99c663954fafc930039201ed07/db/imgs/pdt/angle/PS-SPR-0500NHSAWx-1_58b5d4aad40f4fedab3f7acfc4344cd3.jpg",
		title: "POWER SUPPLIES",
		category: "psu",
	},
	{
		id: 6,
		img: "https://asset.msi.com/resize/image/global/product/product_16291818122a3fa34270501027891bc562ba9db696.png62405b38c58fe0f07fcef2367d8a9ba1/400.png",
		title: "COMPUTER CASE",
		category: "case",
	},
	{
		id: 7,
		img: "https://cdn.shopify.com/s/files/1/0442/2749/4055/products/800x800-Web-SKU-Size1_4a82e9ca-2750-4e34-8762-947dc864b936_1024x1024.png?v=1613527868",
		title: "GAMING MOUSE",
		category: "mouse",
	},
	{
		id: 8,
		img: "https://i0.wp.com/www.pcbworldtech.com/wp-content/uploads/2019/10/Tt-X1-RGB.jpg",
		title: "GAMING KEYBOARDS",
		category: "keyboard",
	},
];

export const popularProducts = [
	{
		id: 1,
		img: "https://d3o2e4jr3mxnm3.cloudfront.net/Mens-Jake-Guitar-Vintage-Crusher-Tee_68382_1_lg.png",
	},
	{
		id: 2,
		img: "https://freepngimg.com/thumb/polo%20shirt/25-polo-shirt-png-image.png",
	},
	{
		id: 3,
		img: "https://freepngimg.com/thumb/dress%20shirt/17-dress-shirt-png-image.png",
	},
	{
		id: 4,
		img: "https://www.burdastyle.com/pub/media/catalog/product/cache/7bd3727382ce0a860b68816435d76e26/107/BUS-PAT-BURTE-1320516/1170x1470_BS_2016_05_132_front.png",
	},
	{
		id: 5,
		img: "https://images.ctfassets.net/5gvckmvm9289/3BlDoZxSSjqAvv1jBJP7TH/65f9a95484117730ace42abf64e89572/Noissue-x-Creatsy-Tote-Bag-Mockup-Bundle-_4_-2.png",
	},
	{
		id: 6,
		img: "https://d3o2e4jr3mxnm3.cloudfront.net/Rocket-Vintage-Chill-Cap_66374_1_lg.png",
	},
	{
		id: 7,
		img: "https://freepngimg.com/thumb/women_bag/134785-blue-handbag-butterfly-hq-image-free.png",
	},
	{
		id: 8,
		img: "https://freepngimg.com/thumb/jacket/27-red-jacket-png-image.png",
	},
	{
		id: 9,
		img: "https://freepngimg.com/thumb/women%20bag/14-women-bag-png-image.png",
	},
	{
		id: 10,
		img: "https://freepngimg.com/thumb/tshirt/26-t-shirt-png-image.png",
	},
];
