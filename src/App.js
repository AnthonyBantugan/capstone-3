import "./App.css";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import { useEffect, useState } from "react";
import Register from "./pages/Register";
import Admin from "./pages/Admin";
import Dashboard from "./pages/Dashboard";
import AdminUsers from "./pages/AdminUsers";
import AdminProducts from "./pages/AdminProducts";
import AdminProduct from "./pages/AdminProduct";
import Home from "./pages/Home";
import Login from "./pages/Login";
import Products from "./pages/Products";
import Product from "./pages/Product";
import Profile from "./pages/Profile";
import Cart from "./pages/Cart";
import Order from "./pages/Order";
import Logout from "./pages/Logout";
import Error from "./pages/Error";
import NavBar from "./components/NavBar";
import Footer from "./components/Footer";
import { UserProvider } from "./UserContext";

function App() {
	const [user, setUser] = useState();
	const [badge, setBadge] = useState(0);
	const [cart, setCart] = useState([]);

	useEffect(() => {
		const loggedInUser = localStorage.getItem("user");
		if (loggedInUser) {
			const foundUser = JSON.parse(loggedInUser);
			setUser(foundUser);
		}
	}, []);

	useEffect(() => {
		localStorage.setItem("cart", JSON.stringify(cart));
	});

	return (
		<UserProvider value={{ user, setUser, badge, setBadge, cart, setCart }}>
			<Router>
				<NavBar />
				<Routes>
					<Route exact path="/admin" element={<Admin />} />
					<Route
						exact
						path="/admin/dashboard"
						element={<Dashboard />}
					/>
					<Route exact path="/admin/users" element={<AdminUsers />} />
					<Route
						exact
						path="/admin/products"
						element={<AdminProducts />}
					/>
					<Route
						exact
						path="/admin/products/:productId"
						element={<AdminProduct />}
					/>
					<Route exact path="/" element={<Home />} />
					<Route exact path="/register" element={<Register />} />
					<Route exact path="/login" element={<Login />} />
					<Route exact path="/products" element={<Products />} />
					<Route
						exact
						path="/products/:productId"
						element={<Product />}
					/>
					<Route
						exact
						path="/profile/:userId"
						element={<Profile />}
					/>
					<Route exact path="/cart" element={<Cart />} />
					<Route exact path="/orders/:userId" element={<Order />} />
					<Route exact path="/logout" element={<Logout />} />
					<Route exact path="*" element={<Error />} />
				</Routes>
				<Footer />
			</Router>
		</UserProvider>
	);
}

export default App;
